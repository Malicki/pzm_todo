(function(){
  function h(tag, props, children) {
    var el = document.createElement(tag);
    if(!!props) {
      var keys = Object.keys(props);
      keys.forEach(function(key){
        if(key === 'attr') {
          var attrObj = props[key];
          var attrs = Object.keys(attrObj);
          attrs.forEach(function(attr){
            el.setAttribute(attr, attrObj[attr])
          })
        }
        else {
          el[key] = props[key];
        }

      })
    }

    if(!!children) {
      for(var i=0; i<children.length; i++) {
        var child = children[i];
        if(!!child) {
          if(typeof child == 'string') {
            child = document.createTextNode(child);
          }
          el.appendChild(child);
        }
      }
    }
    return el;
  }

  function getNewBadge(task) {
    if(!task.created) return '';

    var date = new Date(task.created);
    var time = (new Date()).getTime();
    var diff = time - date.getTime();

    if(diff < (60*60*1000))
      return h('span',{className:'new-task-badge'});
    else '';
  }

  function render(state, ctx){
    return h('main',{ className:'todos-main'},[
      h('label',{},[
        h('input',{
          type:'checkbox',
          checked: state.todoOnly,
          onchange: ctx.onTodoOnlyChange
        }), 'Pokaż tylko do zrobienia'
      ]),
      h('label',{},[
        'sortowanie',
        h('select',{
          onchange: ctx.onSort
        },[
          h('option',{value:'id', selected:state.order == 'id'},['domyślne']),
          h('option',{value:'name', selected:state.order == 'name'},['Nazwa']),
          h('option',{value:'done', selected:state.order == 'done'},['Zrobione']),
          h('option',{value:'public', selected:state.order == 'public'},['Publiczne'])
        ]),
        h('select',{
          onchange: ctx.onSortDir
        },[
          h('option',{value:'asc', selected:state.orderAsc},['rosnąco']),
          h('option',{value:'desc', selected:!state.orderAsc},['malejąco'])
        ])]),
      h('ul',{className:'todos-tasks'},
        state.tasks.map(function(task){
          return h('li',{className:'todos-task'},[
            h('label',{ attr: {
              'data-id': task.id,
              'data-done': task.done,
              'data-public': task.public || false
            }},[
              h('input',{
                type:'checkbox',
                onchange: ctx.onTaskChange,
                checked: task.done
              },[]),
              getNewBadge(task),
              task.name + (task.public ? ' - publiczne' : '')
            ])
          ])
        })
      ),
      h('div',{},[
        h('button',{
          type:'button',
          onclick: ctx.prevPage
        }, ['<']),
        h('span',{},[''+(state.page+1)]),
        h('button',{
          type:'button',
          onclick: ctx.nextPage
        }, ['>'])
      ]),
      h('div',{className: 'todos-actions'},[
        h('label',{},['zadanie',
        h('input',{
          className:'todos-new-task',
          value: state.text,
          onchange: ctx.onTextChange
        }),
        h('label',{},[
          'Powiadom',
          h('input',{
            type: 'email',
            className:'todos-new-task',
            value: state.text,
            onchange: ctx.onNotifyChange
          })
        ])
      ]),
      h('label',{},[
        h('input',{
          type:'checkbox',
          checked: state.public,
          onchange: ctx.onPublicChange
        }), 'Publiczne'
      ]),
      h('button', {
        className: 'todos-add-task',
        onclick: ctx.addTask
      },
      ['Dodaj'])
      ])
    ]);
  }

  function data() {
    return {
      text: '',
      todoOnly: false,
      skiptokens: [],
      order: 'id',
      orders: ['name','done'],
      orderAsc: true,
      public: false,
      page: 0,
      tasks: [],
      notify: ''
    }
  }

  function saveTaskToServerAsync(task) {
    return new Promise(function(resolve,reject) {
      setTimeout(function(){
        if(!window.localStorage['todos']) {
          window.localStorage['todos'] = "[]";
        }
        var tasks = JSON.parse(window.localStorage['todos']);
        var newId = tasks.length;
        
        if(tasks.filter(function(t){
          return t.name == task.name;
        }).length > 0 ) {
          reject("serwer: masz już zadanie o takiej nazwie");
        }
        else {
          tasks.push({
            name: task.name,
            done: task.done,
            public: task.public,
            created: (new Date()).toISOString(),
            notify: task.notify || '',
            id: newId
          });
  
          window.localStorage['todos'] = JSON.stringify(tasks);
  
          resolve({
            name: task.name,
            id: newId
          });
        }

        
      },500);
      

    })
    
  }

  var maxTop = 10;

  function getTasksFromServerAsync(config){
    return new Promise(function(resolve,reject){
      setTimeout(function(){
        if(!window.localStorage['todos']) {
          window.localStorage['todos'] = "[]";
        }
        var tasks = JSON.parse(window.localStorage['todos']);
        var skip = 0;
        var top = config.top || maxTop;
        if(!!config) {
          var filter = config.filter;
          if(!!filter) {
            tasks = tasks.filter(function(task){
              return task.done == filter.done;
            })
          }
          tasks = tasks.sort(function(a,b){
            if(config.sortAsc)
              if(a[config.sort] > b[config.sort])
                return 1;
              else if(a[config.sort] < b[config.sort])
                return -1;
              else return 0;
            else
              if(a[config.sort] > b[config.sort])
                return -1;
              else if(a[config.sort] < b[config.sort])
                return 1;
              else return 0;
          })
          skip = (!!config.skiptoken) ? Number(config.skiptoken) : 0;
           {
            var returnSkiptoken = ((skip+top) < tasks.length );
            tasks = tasks.slice(skip);
          }

          
        }
        
        tasks = tasks.slice(0, top);
        var response = {
          value: tasks
        };
        if(returnSkiptoken) {
          response.skiptoken = skip+top;
        }
        resolve(response);
      },200);
    });
  }

  function patchTaskAsync(id,partialTask) {
    return new Promise(function(resolve,reject){
      setTimeout(function(){
        if(!window.localStorage['todos']) {
          window.localStorage['todos'] = "[]";
        }
        var tasks = JSON.parse(window.localStorage['todos']);
        var task = tasks.filter(function(task){
          return task.id == id;
        });
        if(task.length >= 1)
          task = task[0];
        else task = null;

        if(!!task) {
          var properties = Object.keys(partialTask);
          properties.forEach(function(prop){
            task[prop] = partialTask[prop];
          })
          task.modified = (new Date()).toISOString();

          if(task.notify && task.done) {
            alert('użytkownik '+task.notify+' został powiadomiony o zrealizowaniu zadania');
          }
        }

        window.localStorage['todos'] = JSON.stringify(tasks);

        resolve();
      },200);
    });
  }


  function MainComponent(parent) {
    var self = this;
    
    self.parent = parent;
    self.state = data();

    self.onNotifyChange = function(ev){
      self.state.notify = ev.target.value;
    }

    self.onSort = function(ev){
      var select = ev.target;
      self.state.order = select.selectedOptions[0].value;
      self.state.skiptokens = [];
      self.state.page = 0;
      self.load();
    }
    self.onSortDir = function(ev){
      var select = ev.target;
      self.state.skiptokens = [];
      self.state.page = 0;
      self.state.orderAsc = select.selectedOptions[0].value == 'asc';
      self.load();
    }

    self.onPublicChange = function(ev){
      self.state.public = ev.target.checked;

    }


    self.render = function() {
      var r = render(self.state, self);
      if(!self.el) {
        self.parent.appendChild(r);
      }
      else {
        self.parent.replaceChild(r, self.el);
      }
      self.el = r;
    }

    self.load = function() {
      var todoOnly = self.state.todoOnly;
      var config = {
        sort : self.state.order,
        sortAsc: self.state.orderAsc
      };


      if(todoOnly) {
        config.filter = {
          done: false,
        }
      }

      if(self.state.page > 0) {
        config.skiptoken = self.state.skiptokens[self.state.skiptokens.length-1];
      }

      getTasksFromServerAsync(config).then(function(response){
        self.state.tasks = response.value;
        if(response.skiptoken) {
          self.state.skiptokens.push(response.skiptoken);
        }
        self.render();
      })
    }

    self.onTodoOnlyChange = function(ev){
      self.state.todoOnly = ev.target.checked;
      self.load();
    }

    self.validateNewTask = function(task) {
      return task.name && task.name.length > 0 && task.name.length < 50;
    }

    self.nextPage = function(){
      if(self.state.skiptokens.length == (self.state.page+1)) {
        self.state.page =self.state.page +1;
        self.load();
      }  
      
    }
    self.prevPage = function(){
      if(self.state.page > 0) {
        self.state.page =self.state.page -1;
        self.state.skiptokens.pop();
        self.state.skiptokens.pop();
        self.load();
      }
    }

    self.addTask = function() {
      var task = {
        name: self.state.text,
        done: false,
        public: self.state.public,
        notify: self.state.notify
      };

      if(self.validateNewTask(task)) {
        saveTaskToServerAsync(task).then(function(task){
          //self.state.tasks.push(task);
          self.state.skiptokens.pop();
          self.load();
          alert('Zadanie '+task.name+' zostało dodane ('+task.id+')');
        }).catch(function(comment){
          alert(comment);
        })
        
        self.state.text = '';
        self.state.notify = '';
        self.state.public = false;
      }
      else {
        alert('Zadanie musi mieć minimum 1 znak i maksymalnie 49 znaków.');
      }

    }

    self.onTaskChange = function(ev){
      var checked = ev.target.checked;
      var taskid = Number(ev.target.parentElement.getAttribute('data-id'));

      patchTaskAsync(taskid,{
        done: checked
      }).then(function(){
        self.load();
      })
    }

    self.onTextChange = function(ev){
      self.state.text = ev.target.value;
    }

    self.load();
  }


  //init
  var main = new MainComponent(document.body);
  main.render();


})()